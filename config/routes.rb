Rails.application.routes.draw do
  

  get 'cart/index'

  devise_for :users
  resources :items
  resources :charges
  resources :searches
  root 'pages#home'
  
  get '/home' => 'pages#home'
  get '/about' => 'pages#about'
  get '/contact' => 'pages#contact'
  
  get '/items' => 'items#index'
  
  get '/cart' => 'cart#index'
  get '/cart/clear' => 'cart#clearCart'
  get '/cart/:id' => 'cart#add'
  get '/cart/checkout' => 'cart#checkout'
  
  
 
# youtube link https://www.youtube.com/watch?v=pNL4VPdru0Q

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
